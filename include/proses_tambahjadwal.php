<?php
// include file koneksi.php
include 'koneksi.php';

// Untuk memeriksa apakah variabel simpan telah tersedia atau belum
if (isset($_POST['simpan']))
	{
    // Menetapkan nilai dari setiap variabel
    $id_dosen = $_POST['id_dosen'];
    $id_kelas = $_POST['id_kelas'];
    $jadwal = $_POST['jadwal'];
    $mata_kuliah = $_POST['mata_kuliah'];
    
    // Menginputakan data ke tabel jadwal kelas sesuai dengan data yang telah di inputkan kecuali id dosen dan id kelas
    $query= mysqli_query($koneksi, "INSERT INTO jadwal_kelas VALUES(NULL, '$id_dosen','$id_kelas','$jadwal','$mata_kuliah')");
    
        // Kondisi ketika query berhasil dan tidak
        if($query){
            // Menampilkan pesan berhasil dan mengalihkan ke halaman jadwalkelas.php
            echo "<script>alert('Berhasil Menambahkan Data Jadwal Kelas!');
            window.location.href='../page/jadwalkelas.php';</script>";
            // Menampilkan pesan gagal dan mengalihkan ke halaman editjadwal.php
        }else{
            echo "<script>alert(Gagal Menyimpan Data Jadwal Kelas!');
            window.location.href='../page/tambahjadwal.php';</script>";
        }
}else{
    // Mengalihkan ke halaman jadwalkelas.php (ketika variabel simpan tidak tersedia)
    echo '<script>window.location.href="../page/jadwalkelas.php";</script>';
}
?>