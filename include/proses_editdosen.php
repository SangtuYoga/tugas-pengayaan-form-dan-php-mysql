<?php
// include file koneksi.php
include 'koneksi.php';

// Untuk memeriksa apakah variabel simpan telah tersedia atau belum
if (isset($_POST['simpan']))
	{
	// Menetapkan nilai dari setiap variabel
	$id = $_POST['id'];
	$nip = $_POST['nip'];
	$nama = $_POST['nama'];
	$prodi = $_POST['prodi'];
	$fakultas = $_POST['fakultas'];
	
	$rand = rand();
	$ekstensi =  array('png','jpg','jpeg');
	$filename = $_FILES['foto']['name'];
	$ukuran = $_FILES['foto']['size'];
	$tmp = $_FILES['foto']['tmp_name'];
	$ext = pathinfo($filename, PATHINFO_EXTENSION);
	
	// Kondisi ketika inputan variabel $filename tidak kosong
	if($filename != ""){
		// Mengambil data foto dosen yang terdapat dalam tabel dosen sesuai dengan id dosen
		$get_foto = "SELECT foto_dosen FROM dosen WHERE id_dosen='$id'";
		$data_foto = mysqli_query($koneksi, $get_foto);
		$foto_lama = mysqli_fetch_array($data_foto); 
		// menghapus file foto yang terdapat dalam folder
		unlink("../images/".$foto_lama['foto_dosen']);
		// Kondisi ketika inputan file tidak sesuai dengan ekstensi
		if(!in_array($ext,$ekstensi) ) {
			echo "<script>alert('Gagal Mengubah Data Dosen, Ekstensi File Salah!');
			window.location.href='../page/editdosen.php';</script>";
		}else{
			//Kondisi ketika inputan file melebihi 1MB
			if($ukuran < 1044070){
				// Menetapkan nilai variabel $xx menggunakan variabel $rand + _ + $filename
				$xx = $rand.'_'.$filename;
				// Memindahkan file yang di upload ke lokasi yang sudah ditentukan
				move_uploaded_file($tmp, '../images/'.$rand.'_'.$filename);
				// Mengupdate seluruh data di dalam tabel dosen sesuai dengan data yang telah di inputkan
				mysqli_query($koneksi, "UPDATE dosen SET foto_dosen='$xx', nip_dosen='$nip', nama_dosen='$nama', prodi='$prodi', fakultas='$fakultas' WHERE id_dosen='$id'");
				// Menampilkan pesan berhasil dan mengalihkan ke halaman dosen.php
				echo "<script>alert('Berhasil Menambahkan Data Dosen!');
				window.location.href='../page/dosen.php';</script>";
			}else{
				// Menampilkan pesan gagal dan mengalihkan ke halaman editdosen.php
				echo "<script>alert(Gagal Mengubah Data Dosen, Ukuran File Melebihi 1 MB!');
				window.location.href='../page/editdosen.php';</script>";
			}
		}
	}else{
		// Mengupdate data di dalam tabel dosen sesuai dengan data yang telah di inputkan kecuali foto_dosen
		mysqli_query($koneksi, "UPDATE dosen SET nip_dosen='$nip', nama_dosen='$nama', prodi='$prodi', fakultas='$fakultas' WHERE id_dosen='$id'");
		// Menampilkan pesan berhasil dan mengalihkan ke halaman dosen.php
		echo "<script>alert('Berhasil Mengubah Data Dosen!');
		window.location.href='../page/dosen.php';</script>";
	}
}else{
	// Mengalihkan ke halaman dosen.php (ketika variabel simpan tidak tersedia)
    echo '<script>window.location.href="../page/dosen.php";</script>';
}
?>