<?php
// include file koneksi.php
include 'koneksi.php';

// Untuk memeriksa apakah variabel simpan telah tersedia atau belum
if (isset($_POST['simpan']))
	{
    // Menetapkan nilai dari setiap variabel
    $nama_kelas = $_POST['nama_kelas'];
    $prodi = $_POST['prodi'];
    $fakultas = $_POST['fakultas'];
    
     // Menginputkan seluruh data ke tabel kelas sesuai dengan data yang telah di inputkan
    $query= mysqli_query($koneksi, "INSERT INTO kelas VALUES(NULL, '$nama_kelas','$prodi','$fakultas')");
    
        // Kondisi ketika query berhasil dan tidak
        if($query){
            // Menampilkan pesan berhasil dan mengalihkan ke halaman kelas.php
            echo "<script>alert('Berhasil Menambahkan Data Kelas!');
            window.location.href='../page/kelas.php';</script>";
        }else{
            // Menampilkan pesan gagal dan mengalihkan ke halaman editkelas.php
            echo "<script>alert(Gagal Menyimpan Data Kelas!');
            window.location.href='../page/tambahkelas.php';</script>";
        }
}else{
    // Mengalihkan ke halaman kelas.php (ketika variabel simpan tidak tersedia)
    echo '<script>window.location.href="../page/kelas.php";</script>';
}
?>