<?php
// include file koneksi.php
include 'koneksi.php';

// Untuk memeriksa apakah variabel simpan telah tersedia atau belum
if (isset($_POST['simpan']))
	{
    // Menetapkan nilai dari setiap variabel
    $id = $_POST['id'];
    $id_dosen = $_POST['id_dosen'];
    $id_kelas = $_POST['id_kelas'];
    $jadwal = $_POST['jadwal'];
    $mata_kuliah = $_POST['mata_kuliah'];
    
    //Kondisi ketika inputan variabel $jadwal dan $mata_kuliah tidak kosong
    if($jadwal && $mata_kuliah !=""){
        // Mengupdate data di dalam tabel jadwal kelas sesuai dengan data yang telah di inputkan kecuali id dosen dan id kelas
        $query=mysqli_query($koneksi, "UPDATE jadwal_kelas SET  jadwal='$jadwal', mata_kuliah='$mata_kuliah' WHERE id_jadwal='$id'");

        // Kondisi ketika query berhasil dan tidak
        if($query){
            // Menampilkan pesan berhasil dan mengalihkan ke halaman jadwalkelas.php
            echo "<script>alert('Berhasil Mengubah Data Jadwal Kelas!');
            window.location.href='../page/jadwalkelas.php';</script>";
        }else{
            // Menampilkan pesan gagal dan mengalihkan ke halaman editjadwal.php
            echo "<script>alert(Gagal Mengubah Data Jadwal Kelas!');
            window.location.href='../page/editjadwal.php';</script>";
        }
    }else{
        // Mengupdate seluruh data di dalam tabel dosen sesuai dengan data yang telah di inputkan
        $query=mysqli_query($koneksi, "UPDATE jadwal_kelas SET id_dosen='$id_dosen', id_kelas='$id_kelas', jadwal='$jadwal', mata_kuliah='$mata_kuliah' WHERE id_jadwal='$id'");

         // Kondisi ketika query berhasil dan tidak
        if($query){
            // Menampilkan pesan berhasil dan mengalihkan ke halaman jadwalkelas.php
            echo "<script>alert('Berhasil Mengubah Data Jadwal Kelas!');
            window.location.href='../page/jadwalkelas.php';</script>";
        }else{
            // Menampilkan pesan gagal dan mengalihkan ke halaman editjadwal.php
            echo "<script>alert(Gagal Mengubah Data Jadwal Kelas!');
            window.location.href='../page/editjadwal.php';</script>";
        }
    }

}else{
    // Mengalihkan ke halaman jadwalkelas.php (ketika variabel simpan tidak tersedia)
    echo '<script>window.location.href="../page/jadwalkelas.php";</script>';
}
?>