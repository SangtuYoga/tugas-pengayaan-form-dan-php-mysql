<?php
// include file koneksi.php
include 'koneksi.php';

// Untuk memeriksa apakah variabel simpan telah tersedia atau belum
if (isset($_POST['simpan']))
	{
    // Menetapkan nilai dari setiap variabel
    $nip = $_POST['nip'];
    $nama = $_POST['nama'];
    $prodi = $_POST['prodi'];
    $fakultas = $_POST['fakultas'];
    
    $rand = rand();
    $ekstensi =  array('png','jpg','jpeg');
    $filename = $_FILES['foto']['name'];
    $ukuran = $_FILES['foto']['size'];
    $tmp = $_FILES['foto']['tmp_name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    // Kondisi ketika inputan file tidak sesuai dengan ekstensi
    if(!in_array($ext,$ekstensi) ) {
        echo "<script>alert('Gagal Menyimpan Data Dosen, Ekstensi File Salah!');
        window.location.href='../page/tambahdosen.php';</script>";
    }else{
        //Kondisi ketika inputan file melebihi 1MB
        if($ukuran < 1044070){
            // Menetapkan nilai variabel $xx menggunakan variabel $rand + _ + $filename		
            $xx = $rand.'_'.$filename;
            // Memindahkan file yang di upload ke lokasi yang sudah ditentukan
            move_uploaded_file($tmp, '../images/'.$rand.'_'.$filename);
            // Menginputkan seluruh data ke tabel dosen sesuai dengan data yang telah di inputkan
            mysqli_query($koneksi, "INSERT INTO dosen VALUES(NULL,'$xx','$nip','$nama','$prodi','$fakultas')");
            // Menampilkan pesan berhasil dan mengalihkan ke halaman dosen.php
            echo "<script>alert('Berhasil Menambahkan Data Dosen!');
            window.location.href='../page/dosen.php';</script>";
        }else{
            // Menampilkan pesan gagal dan mengalihkan ke halaman editdosen.php
            echo "<script>alert(Gagal Menyimpan Data Dosen, Ukuran File Melebihi 1 MB!');
            window.location.href='../page/tambahdosen.php';</script>";
        }
    }
}else{
    // Mengalihkan ke halaman dosen.php (ketika variabel simpan tidak tersedia)
    echo '<script>window.location.href="../page/dosen.php";</script>';
}
?>