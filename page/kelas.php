<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Kelas_1915091020</title>
</head>

<body>

    <div class="container-fluid">
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                <div class="d-flex flex-column align-items-center align-items-sm-start text-white vh-100">
                    <a href="dashboard.php"
                        class="d-flex align-items-center pb-3 mb-md-2 me-md-auto text-white text-decoration-none">
                        <span class="fs-5 d-none d-sm-inline navbar navbar-expand-lg navbar-dark bg-dark">Dashboard</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start ">
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard.php">
                                Beranda
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dosen.php">
                                Dosen
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="kelas.php">
                                Kelas <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="jadwalkelas.php">
                                Jadwal Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.html">
                                Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col py-3">
                <h1 class="display-5 fw-normal">Data Kelas</h1>
                <a href="tambahkelas.php" class="btn btn-primary" role="button" data-bs-toggle="button">Tambah Data</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Kelas</th>
                            <th scope="col">Program Studi</th>
                            <th scope="col">Fakultas</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        // include file koneksi.php
                        include('../include/koneksi.php');

                        // Mengambil seluruh data yang ada di dalam tabel kelas
                        $query = mysqli_query($koneksi, "SELECT * FROM kelas");

                        //cek, apakakah hasil query di atas mendapatkan hasil atau tidak (data kosong atau tidak)
                        if(mysqli_num_rows($query) == 0){	//ini artinya jika data hasil query di atas kosong
                            
                            //jika data kosong, maka akan menampilkan row kosong
                            echo '<tr><td colspan="5">Tidak ada data!</td></tr>';
                            
                        }else{	//else ini artinya jika data hasil query ada (data diu database tidak kosong)
                            
                            //jika data tidak kosong, maka akan melakukan perulangan while
                            $no = 1;	//membuat variabel $no untuk membuat nomor urut
                            while($data = mysqli_fetch_array($query)){	//perulangan while dg membuat variabel $data yang akan mengambil data di database
                                
                                //menampilkan row dengan data di database
                                echo '<tr>';
                                    //menampilkan nomor urut
                                    echo '<td>'.$no.'</td>';
                                    //menampilkan data nama kelas dari database	
                                    echo '<td>'.$data['nama_kelas'].'</td>';
                                    //menampilkan data prodi dari database	
                                    echo '<td>'.$data['prodi'].'</td>';
                                    //menampilkan data fakultas dari database	
                                    echo '<td>'.$data['fakultas'].'</td>';
                                    //menampilkan link edit dan hapus dimana tiap link terdapat GET id -> ?id=id_kelas
                                    echo '<td>
                                    <a class="btn btn-primary" href="editkelas.php?id_kelas='.$data['id_kelas'].'">Edit</a>
                                    <a class="btn btn-danger" href="../include/proses_deletekelas.php?id_kelas='.$data['id_kelas'].'" onclick="return confirm(\'Yakin ingin menghapus?\')">Hapus</a></td>';
                                echo '</tr>';
                                
                                $no++;	//menambah jumlah nomor urut setiap row
                                
                            }
                            
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>