<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Dosen_1915091020</title>
</head>

<body>

    <div class="container-fluid">
        <!-- Sidebar / Menu -->
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                <div class="d-flex flex-column align-items-center align-items-sm-start text-white vh-100">
                    <a href="dashboard.php"
                        class="d-flex align-items-center pb-3 mb-md-2 me-md-auto text-white text-decoration-none">
                        <span class="fs-5 d-none d-sm-inline navbar navbar-expand-lg navbar-dark bg-dark">Dashboard</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start ">
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard.php">
                                Beranda
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="dosen.php">
                                Dosen <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="kelas.php">
                                Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="jadwalkelas.php">
                                Jadwal Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.html">
                                Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Content yang ada di dalam page ini -->
            <div class="col py-3">
                <h1 class="display-5 fw-normal">Data Dosen</h1>
                <a href="tambahdosen.php" class="btn btn-primary" role="button" data-bs-toggle="button">Tambah Data</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Foto</th>
                            <th scope="col">NIP</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Program Studi</th>
                            <th scope="col">Fakultas</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        // include file koneksi.php
                        include('../include/koneksi.php');

                        // Mengambil seluruh data yang ada di dalam tabel dosen
                        $query = mysqli_query($koneksi, "SELECT * FROM dosen");

                        //cek, apakakah hasil query di atas mendapatkan hasil atau tidak (data kosong atau tidak)
                        if(mysqli_num_rows($query) == 0){	//ini artinya jika data hasil query di atas kosong
                            
                            //jika data kosong, maka akan menampilkan row kosong
                            echo '<tr><td colspan="7">Tidak ada data!</td></tr>';
                            
                        }else{	//else ini artinya jika data hasil query ada (data di database tidak kosong)
                            
                            //jika data tidak kosong, maka akan melakukan perulangan while
                            $no = 1;	//membuat variabel $no untuk membuat nomor urut
                            while($data = mysqli_fetch_array($query)){	//perulangan while dg membuat variabel $data yang akan mengambil data di database
                                
                                //menampilkan row dengan data di database
                                echo '<tr>';
                                    //menampilkan nomor urut
                                    echo '<td>'.$no.'</td>';
                                    //menampilkan data foto dosen dari database	
                                    echo '<td><img src="../images/'.$data['foto_dosen'].'" width="75" height="100"></td>';	
                                    //menampilkan data nip dosen dari database
                                    echo '<td>'.$data['nip_dosen'].'</td>';
                                    //menampilkan data nama dosen dari database	
                                    echo '<td>'.$data['nama_dosen'].'</td>';///menampilkan data prodi dari database	
                                    echo '<td>'.$data['prodi'].'</td>';
                                    //menampilkan data fakultas dari database	
                                    echo '<td>'.$data['fakultas'].'</td>';
                                    //menampilkan link edit dan hapus dimana tiap link terdapat GET id -> ?id=id_dosen
                                    echo '<td>
                                    <a class="btn btn-primary" href="editdosen.php?id_dosen='.$data['id_dosen'].'">Edit</a>
                                    <a class="btn btn-danger" href="../include/proses_deletedosen.php?id_dosen='.$data['id_dosen'].'" onclick="return confirm(\'Yakin ingin menghapus?\')">Hapus</a></td>';
                                echo '</tr>';
                                
                                $no++;	//menambah jumlah nomor urut setiap row
                                
                            }
                            
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>