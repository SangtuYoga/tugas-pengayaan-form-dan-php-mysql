<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Tambah Dosen_1915091020</title>
</head>

<body>

    <div class="container-fluid">
        <!-- Sidebar / Menu -->
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                <div class="d-flex flex-column align-items-center align-items-sm-start text-white vh-100">
                    <a href="dashboard.php"
                        class="d-flex align-items-center pb-3 mb-md-2 me-md-auto text-white text-decoration-none">
                        <span class="fs-5 d-none d-sm-inline navbar navbar-expand-lg navbar-dark bg-dark">Dashboard</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start ">
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard.php">
                                Beranda
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="dosen.php">
                                Dosen <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="kelas.php">
                                Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="jadwalkelas.php">
                                Jadwal Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.html">
                                Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Content yang ada di dalam page ini -->
            <div class="col py-3">
                <h1 class="display-5">Tambah Data Dosen</h1>
                <form action="../include/proses_tambahdosen.php" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="nip" class="form-label"><b>NIP :</b></label>
                        <input type="number" name="nip" class="form-control" id="nip" required>
                    </div>
                    <div class="mb-3">
                        <label for="nama" class="form-label"><b>Nama :</b></label>
                        <input type="text" name="nama" class="form-control" id="nama" required>
                    </div>
                    <div class="mb-3">
                        <label for="prodi" class="form-label"><b>Program Studi :</b></label>
                        <input type="text" name="prodi" class="form-control" id="prodi" required>
                    </div>
                    <div class="mb-3">
                        <label for="fakultas" class="form-label"><b>Fakultas :</b></label>
                        <input type="text" name="fakultas" class="form-control" id="fakultas" required>
                    </div>
                    <div class="form-group">
                        <label for="formFile" class="form-label"><b>Foto :</b></label>
                        <input class="form-control" type="file" name="foto" id="formFile" required>
                        <p style="color: red">Ekstensi yang diperbolehkan .png | .jpg | .jpeg</p>
                    </div>
                    <input type="submit" class="btn btn-danger" value="Batal" onclick="window.location='dosen.php';">
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>
</body>

</html>