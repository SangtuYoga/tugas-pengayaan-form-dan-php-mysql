<?php
// include file koneksi.php 
include '../include/koneksi.php';

// Untuk memeriksa apakah variabel id_kelas telah tersedia atau belum
if (isset($_GET['id_kelas'])) {

    // Kondisi ketika variabel id_kelas tidak kosong
	if ($_GET['id_kelas'] != "") {
		
        //Membuat variabel $id yg nilainya adalah dari URL GET id -> editkelas.php?id=id_kelas
		$id = $_GET['id_kelas'];

        //Melakukan query ke database dg SELECT table kelas dengan kondisi WHERE id_kelas = '$id'
		$query = mysqli_query($koneksi,"SELECT * FROM kelas WHERE id_kelas='$id'");
		$row = mysqli_fetch_array($query);

	}else{
        //Untuk meredirect ke kelas.php
		header("location:kelas.php");
	}
}else{
    //Untuk meredirect ke kelas.php
	header("location:kelas.php");
}

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Edit Kelas_1915091020</title>
</head>

<body>

    <div class="container-fluid">
        <!-- Sidebar / Menu -->
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                <div class="d-flex flex-column align-items-center align-items-sm-start text-white vh-100">
                    <a href="dashboard.php"
                        class="d-flex align-items-center pb-3 mb-md-2 me-md-auto text-white text-decoration-none">
                        <span class="fs-5 d-none d-sm-inline navbar navbar-expand-lg navbar-dark bg-dark">Dashboard</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start ">
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard.php">
                                Beranda
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dosen.php">
                                Dosen
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="kelas.php">
                                Kelas <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="jadwalkelas.php">
                                Jadwal Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.html">
                                Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Content yang ada di dalam page ini -->
            <div class="col py-3">
                <h1 class="display-5">Edit Data Kelas</h1>
                <form action="../include/proses_editkelas.php" method="post" enctype="multipart/form-data">
                <!-- Membuat inputan hidden dan nilainya adalah id_kelas -->
                <input type="hidden" name="id" value="<?php echo $row['id_kelas']; ?>">
                    <div class="mb-3">
                        <label for="nama_kelas" class="form-label"><b>Nama Kelas :</b></label>
                        <!-- value diambil dari hasil query -->
                        <input type="text" name="nama_kelas" class="form-control" id="nama_kelas" value="<?php echo $row['nama_kelas']; ?>" required>
                    </div>
                    <div class="mb-3">
                        <label for="prodi" class="form-label"><b>Program Studi :</b></label>
                        <!-- value diambil dari hasil query -->
                        <input type="text" name="prodi" class="form-control" id="prodi" value="<?php echo $row['prodi']; ?>" required>
                    </div>
                    <div class="mb-3">
                        <label for="fakultas" class="form-label"><b>Fakultas :</b></label>
                        <!-- value diambil dari hasil query -->
                        <input type="text" name="fakultas" class="form-control" id="fakultas" value="<?php echo $row['fakultas']; ?>" required>
                    </div>
                    <input type="submit" class="btn btn-danger" value="Batal">
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>
</body>

</html>