<?php 
// include file koneksi.php
include '../include/koneksi.php';

// Untuk memeriksa apakah variabel id_jadwal telah tersedia atau belum
if (isset($_GET['id_jadwal'])) {

    // Kondisi ketika variabel id_jadwal tidak kosong
	if ($_GET['id_jadwal'] != "") {
		
        //Membuat variabel $id yg nilainya adalah dari URL GET id -> editjadwal.php?id=id_jadwal
		$id = $_GET['id_jadwal'];
        
        //Melakukan query ke database dg SELECT table jadwal_kelas dengan kondisi WHERE id_jadwal = '$id'
        $query = mysqli_query($koneksi, "SELECT jadwal_kelas.*, dosen.nama_dosen, kelas.nama_kelas FROM jadwal_kelas, dosen, kelas WHERE dosen.id_dosen=jadwal_kelas.id_dosen AND kelas.id_kelas = jadwal_kelas.id_kelas AND id_jadwal='$id'");
		$row = mysqli_fetch_array($query);

	}else{
        //Untuk meredirect ke jadwalkelas.php
		header("location:jadwalkelas.php");
	}
}else{
    //Untuk meredirect ke jadwalkelas.php
	header("location:jadwalkelas.php");
}

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Edit Jadwal_1915091020</title>
</head>

<body>

    <div class="container-fluid">
        <!-- Sidebar / Menu -->
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                <div class="d-flex flex-column align-items-center align-items-sm-start text-white vh-100">
                    <a href="dashboard.php"
                        class="d-flex align-items-center pb-3 mb-md-2 me-md-auto text-white text-decoration-none">
                        <span class="fs-5 d-none d-sm-inline navbar navbar-expand-lg navbar-dark bg-dark">Dashboard</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start ">
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard.php">
                                Beranda
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dosen.php">
                                Dosen
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="kelas.php">
                                Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="jadwalkelas.php">
                                Jadwal Kelas <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.html">
                                Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Content yang ada di dalam page ini -->
            <div class="col py-3">
                <h1 class="display-5">Edit Data Jadwal Kelas</h1>
                <form action="../include/proses_editjadwal.php" method="post" enctype="multipart/form-data">
                <!-- Membuat inputan hidden dan nilainya adalah id_dosen -->
                <input type="hidden" name="id" value="<?php echo $row['id_jadwal']; ?>">
                    <div class="mb-3">
                        <label for="nama_kelas" class="form-label"><b>Nama Dosen :</b></label>
                        <p style="color: black">Dosen sekarang:
                        <!-- menampilkan value diambil dari hasil query -->
                        <?php echo $row["nama_dosen"]; ?><br>
                        <span style="color: red">Abaikan jika tidak ingin mengubah!</span>
                        </p>
                        <select class="form-select" name="id_dosen" id="id_dosen">
                        <option selected>--Pilih Dosen--</option>
                        <?php 
                        // Melakukan query ke database dg SELECT seluruh data table dosen
                        $sql=mysqli_query($koneksi, "SELECT * FROM dosen");
                        while ($data=mysqli_fetch_array($sql)) {
                        ?>
                        <!-- menampilkan nama value dari hasil query sesuai dengan id -->
                        <option value="<?php echo $data['id_dosen'] ?>"><?php echo $data['nama_dosen'] ?></option>
                        <?php 
                        }
                        ?>
                    </select>
                    </div>
                    <div class="mb-3">
                        <label for="nama_kelas" class="form-label"><b>Nama Kelas :</b></label>
                        <p style="color: black">Kelas sekarang:
                        <!-- menampilkan value diambil dari hasil query -->
                        <?php echo $row["nama_kelas"]; ?><br>
                        <span style="color: red">Abaikan jika tidak ingin mengubah!</span>
                        </p>
                        <select class="form-select" name="id_kelas" id="id_kelas">
                        <option selected>--Pilih Kelas--</option>
                        <?php 
                        // Melakukan query ke database dg SELECT seluruh data table kelas
                        $sql=mysqli_query($koneksi, "SELECT * FROM kelas");
                        while ($data=mysqli_fetch_array($sql)) {
                        ?>
                        <!-- menampilkan nama value dari hasil query sesuai dengan id -->
                        <option value="<?php echo $data['id_kelas'] ?>"><?php echo $data['nama_kelas'] ?></option>
                        <?php 
                        }
                        ?>
                    </select>
                    </div>
                    <div class="mb-3">
                        <label for="jadwal" class="form-label"><b>Jadwal :</b></label>
                        <p style="color: black">Tanggal sekarang:
                        <!-- menampilkan value diambil dari hasil query -->
                        <?php echo date('d/m/y H:i', strtotime($row["jadwal"])); ?>
                        </p>
                        <input type="datetime-local" name="jadwal" class="form-control" id="jadwal" value="<?php echo $row['jadwal']; ?>" required>
                    </div>
                    <div class="mb-3">
                        <label for="mata_kuliah" class="form-label"><b>Mata Kuliah :</b></label>
                        <!-- menampilkan value diambil dari hasil query -->
                        <input type="text" name="mata_kuliah" class="form-control" id="mata_kuliah" value="<?php echo $row['mata_kuliah']; ?>" required>
                    </div>
                    <input type="submit" class="btn btn-danger" value="Batal">
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>
</body>

</html>