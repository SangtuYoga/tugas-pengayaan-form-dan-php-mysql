<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Tambah Kelas_1915091020</title>
</head>

<body>

    <div class="container-fluid">
        <!-- Sidebar / Menu -->
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                <div class="d-flex flex-column align-items-center align-items-sm-start text-white vh-100">
                    <a href="dashboard.php"
                        class="d-flex align-items-center pb-3 mb-md-2 me-md-auto text-white text-decoration-none">
                        <span class="fs-5 d-none d-sm-inline navbar navbar-expand-lg navbar-dark bg-dark">Dashboard</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start ">
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard.php">
                                Beranda
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dosen.php">
                                Dosen
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="kelas.php">
                                Kelas <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="jadwalkelas.php">
                                Jadwal Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.html">
                                Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Content yang ada di dalam page ini -->
            <div class="col py-3">
                <h1 class="display-5">Tambah Data Kelas</h1>
                <form action="../include/proses_tambahkelas.php" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="nama_kelas" class="form-label"><b>Nama Kelas :</b></label>
                        <input type="text" name="nama_kelas" class="form-control" id="nama_kelas" required>
                    </div>
                    <div class="mb-3">
                        <label for="prodi" class="form-label"><b>Program Studi :</b></label>
                        <input type="text" name="prodi" class="form-control" id="prodi" required>
                    </div>
                    <div class="mb-3">
                        <label for="fakultas" class="form-label"><b>Fakultas :</b></label>
                        <input type="text" name="fakultas" class="form-control" id="fakultas" required>
                    </div>
                    <input type="submit" class="btn btn-danger" value="Batal" onclick="window.location='kelas.php';">
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>
</body>

</html>