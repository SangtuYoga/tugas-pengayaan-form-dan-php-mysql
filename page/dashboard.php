<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Dashboard_1915091020</title>
</head>

<body>
    <div class="container-fluid">
        <!-- Sidebar / Menu -->
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                <div class="d-flex flex-column align-items-center align-items-sm-start text-white vh-100">
                    <a href="dashboard.php"
                        class="d-flex align-items-center pb-3 mb-md-2 me-md-auto text-white text-decoration-none">
                        <span
                            class="fs-5 d-none d-sm-inline navbar navbar-expand-lg navbar-dark bg-dark">Dashboard</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start ">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">
                                Beranda <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dosen.php">Dosen
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="kelas.php">
                                Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="jadwalkelas.php">
                                Jadwal Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.html">
                                Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Content yang ada di dalam page ini -->
            <div class="col py-3">
                <h1 class="display-3 fw-normal">Dashboard Sistem Penjadwalan Dosen</h1>
                <p class="lead fw-normal">Selamat Datang di Dashboard Sistem Penjadwalan Dosen</p>
            </div>
        </div>
    </div>
</body>

</html>