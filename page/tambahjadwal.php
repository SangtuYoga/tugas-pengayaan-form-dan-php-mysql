<?php
    // include file koneksi.php
    require_once("../include/koneksi.php");
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Tambah Jadwal_1915091020</title>
</head>

<body>

    <div class="container-fluid">
        <!-- Sidebar / Menu -->
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                <div class="d-flex flex-column align-items-center align-items-sm-start text-white vh-100">
                    <a href="dashboard.php"
                        class="d-flex align-items-center pb-3 mb-md-2 me-md-auto text-white text-decoration-none">
                        <span class="fs-5 d-none d-sm-inline navbar navbar-expand-lg navbar-dark bg-dark">Dashboard</span>
                    </a>
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start ">
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard.php">
                                Beranda
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dosen.php">
                                Dosen
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="kelas.php">
                                Kelas
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="jadwalkelas.php">
                                Jadwal Kelas <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../index.html">
                                Keluar
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- Content yang ada di dalam page ini -->
            <div class="col py-3">
                <h1 class="display-5">Tambah Data Jadwal Kelas</h1>
                <form action="../include/proses_tambahjadwal.php" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="nama_kelas" class="form-label">Nama Dosen :</label>
                        <select class="form-select" name="id_dosen" id="id_dosen">
                        <option selected>--Pilih Dosen--</option>
                        <?php 
                        // Melakukan query ke database dg SELECT seluruh data table dosen
                        $sql=mysqli_query($koneksi, "SELECT * FROM dosen");
                        while ($data=mysqli_fetch_array($sql)) {
                        ?>
                        <!-- menampilkan nama value dari hasil query sesuai dengan id -->
                        <option value="<?php echo $data['id_dosen'] ?>"><?php echo $data['nama_dosen'] ?></option>
                        <?php 
                        }
                        ?>
                    </select>
                    </div>
                    <div class="mb-3">
                        <label for="nama_kelas" class="form-label">Nama Kelas :</label>
                        <select class="form-select" name="id_kelas" id="id_kelas">
                        <option selected>--Pilih Kelas--</option>
                        <?php 
                         // Melakukan query ke database dg SELECT seluruh data table kelas
                        $sql=mysqli_query($koneksi, "SELECT * FROM kelas");
                        while ($data=mysqli_fetch_array($sql)) {
                        ?>
                         <!-- menampilkan nama value dari hasil query sesuai dengan id -->
                        <option value="<?php echo $data['id_kelas'] ?>"><?php echo $data['nama_kelas'] ?></option>
                        <?php 
                        }
                        ?>
                    </select>
                    </div>
                    <div class="mb-3">
                        <label for="jadwal" class="form-label">Jadwal :</label>
                        <input type="datetime-local" name="jadwal" class="form-control" id="jadwal" required>
                    </div>
                    <div class="mb-3">
                        <label for="mata_kuliah" class="form-label">Mata Kuliah :</label>
                        <input type="text" name="mata_kuliah" class="form-control" id="mata_kuliah" required>
                    </div>
                    <input type="submit" class="btn btn-danger" value="Batal" onclick="window.location='jadwalkelas.php';">
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>
</body>

</html>